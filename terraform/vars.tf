
variable "project_id" {}

variable "region" {} 

variable "envprefix" {
  validation {
    condition     = length(var.envprefix) < 15
    error_message = "The envprefix value must be 14 chars long at max."
  }    
}

variable "pgsql_password" {}

variable "gke_location" {
    description = "Required to setup zonal cluster. i.e us-central1-c"
}



