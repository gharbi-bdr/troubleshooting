# Setup for Postgresql instance in private setting.

resource "google_compute_global_address" "private_ip_address" {
  provider                = google-beta

  name                    = "${var.envprefix}-privateaddress"
  purpose                 = "VPC_PEERING"
  address_type            = "INTERNAL"
  prefix_length           = 16
  network                 = var.vpc_id
}

resource "google_service_networking_connection" "private_vpc_connection" {
  provider = google-beta

  network                 = var.vpc_id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

resource "random_id" "db_name_suffix" {
  byte_length             = 5
}

resource "google_sql_database" "main_db" {
  name     = "main-db"
  instance = google_sql_database_instance.pgsql.name
}


# Private pgsql setup.
resource "google_sql_database_instance" "pgsql" {
  database_version        = "POSTGRES_13"

  name                    = "postgresql-priv-${var.envprefix}-${random_id.db_name_suffix.hex}"
  project                 = var.project_id
  region                  = var.region
  deletion_protection     = false
  depends_on = [google_service_networking_connection.private_vpc_connection]

  settings {
    tier                  =  var.machine_type
    disk_size             =  var.disk_size_gb
    ip_configuration {
      ipv4_enabled        = true
      private_network     = var.vpc_id
      authorized_networks {
        name = "dev access."
        value = "0.0.0.0/0"
      }
    }
  }
}

resource "google_sql_user" "pgsql_user" {
  name                    = "main-db-admin"
  instance                = google_sql_database_instance.pgsql.name
  password                = var.pgsql_password
}
