variable "network_name" {}

variable "machine_type" {}

variable "project_id" {}

variable "region" {}

variable "disk_size_gb" {}

variable "envprefix" {}

variable "vpc_id" {}

variable "pgsql_password" {}
