resource "google_compute_network" "vpc" {
  name                              = "${var.envprefix}-vpc"
  routing_mode                      = "GLOBAL"
  auto_create_subnetworks           = "false"
  project                           = var.project_id
}



resource "google_compute_subnetwork" "subnetwork" {
  name                              = "${var.envprefix}-subnetwork"
  ip_cidr_range                     = "10.0.0.0/16"
  network                           = google_compute_network.vpc.id
  region                            = var.region
  private_ip_google_access          = true
  secondary_ip_range {
        range_name                  = "vpc-secondary-ips"
        ip_cidr_range               = "192.168.101.0/24"
  }

  lifecycle {
    ignore_changes                  = [secondary_ip_range]
  }
}

resource "google_compute_route" "route-ilb" {
  name                              = "${var.envprefix}-route-ilb"
  project                           = var.project_id
  dest_range                        = "0.0.0.0/0"
  network                           = google_compute_network.vpc.name
  next_hop_gateway                  = "default-internet-gateway"
  priority                          = 1000
}
