
output "vpc_link" {
  value = google_compute_network.vpc.self_link
}

output "vpc_id" {
  value = google_compute_network.vpc.id
}

output "subnetwork" {
  value = google_compute_subnetwork.subnetwork.name
}

output "network_name" {
  value = google_compute_network.vpc.name
}
