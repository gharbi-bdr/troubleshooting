variable "access_token" {}

variable "cluster_ca_certificate" {}

variable "gke_endpoint" {}

variable "gke_cluster_name" {}

variable "wait_for_gke_setup" {}