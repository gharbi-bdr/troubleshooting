provider "kubernetes" {

  host  = "https://${var.gke_endpoint}"
  token = var.access_token
  cluster_ca_certificate = base64decode(
    var.cluster_ca_certificate,
  )
}

provider "helm"{
   kubernetes{
      host  = "https://${var.gke_endpoint}"
      token = var.access_token
      cluster_ca_certificate = base64decode(
        var.cluster_ca_certificate,
      )
   }
}


# Create a separate namespace to deploy nginx ingress.
resource "kubernetes_namespace" "nginx_ingress_namespace" {
  depends_on = [
    var.wait_for_gke_setup
  ]

  metadata {
    annotations = {
      name = "bspin-nginx-ing-ns"
    }

    labels = {
      app = "nginx"
    }


    name = "bspin-nginx-ing-ns"
  }
}

# Install nginx as ingress controller.
resource "helm_release" "nginx_ingress" {
  provider = helm
  name  = "nginx-ingress"
  chart = "nginx-ingress"
  repository = "https://helm.nginx.com/stable"
  namespace = "bspin-nginx-ing-ns"  

  depends_on = [
    kubernetes_namespace.nginx_ingress_namespace
  ]

}

# Create cert manager namespace
resource "kubernetes_namespace" "cert_manager_namespace" {
  metadata {
    annotations = {
      name = "cert-manager"
    }
    name = "cert-manager"
  }
}

# Install helm release Cert Manager
resource "helm_release" "cert-manager" {
  provider = helm
  name       = "cert-manager"
  chart      = "cert-manager"
  repository = "https://charts.jetstack.io"
  version    = "1.0.4"
  namespace  = kubernetes_namespace.cert_manager_namespace.metadata[0].name

  set {
    name  = "installCRDs"
    value = "true"
  }
}

# Create cert manager namespace
resource "kubernetes_namespace" "ing_cert" {
  metadata {
    annotations = {
      name = "antonn-250525"
    }
    name = "antonn-250525"
  }
}

resource "helm_release" "clusterissuer" {
  name       = "clusterissuer"
  chart      = "./modules/kubernetes/clusterissuer"
  depends_on = [
    helm_release.cert-manager,
    kubernetes_namespace.ing_cert
    ]
}


data "google_client_config" "provider" {}

