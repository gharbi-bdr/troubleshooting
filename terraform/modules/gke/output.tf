output "gke_endpoint" {
  value     =  data.google_container_cluster.clusterinfo.endpoint
}       

output "cluster_ca_certificate" {
  value     = data.google_container_cluster.clusterinfo.master_auth[0].cluster_ca_certificate
}

output "gke_access_token" {
    value     = data.google_client_config.gcpconfig.access_token  
}

output "gcp_node_pool_id" {
    value     = google_container_node_pool.gke_node_pool.id
}