# google_client_config and kubernetes provider must be explicitly specified like the following.
data "google_client_config" "default" {}

resource "google_service_account" "default" {
        account_id                          =   "${var.envprefix}-accountid"
        display_name                        =   "Service Account for ${var.envprefix} space."
}


resource "google_service_account_iam_binding" "default_nodepool_sa" {
  service_account_id = google_service_account.default.name
  role               = "roles/editor"

  members = [
    "serviceAccount:${var.envprefix}-accountid@${var.project_id}.iam.gserviceaccount.com",
  ]
}

resource "google_project_iam_binding" "default_nodepool_sa" {
  project = var.project_id
  role    = "roles/storage.objectAdmin"

  members = [
    "serviceAccount:${var.envprefix}-accountid@${var.project_id}.iam.gserviceaccount.com",
  ]
}


# Create gke cluster with no nodepool attached.
resource "google_container_cluster" "gke_cluster" {
        name                                =   var.gke_cluster_name
        # Regional cluster or Zonal cluster.
        location                            =   var.gke_location
        network                             =   var.network
        subnetwork                          =   var.subnetwork

        master_auth {
            username = ""
            password = ""

            client_certificate_config {
            issue_client_certificate = false
            }
        }

        remove_default_node_pool            =   true
        initial_node_count                  =   1

      cluster_autoscaling {
          enabled = true
          resource_limits {
            resource_type = "cpu"
            minimum = 1
            maximum = 5
          }
          resource_limits {
            resource_type = "memory"
            minimum = 1
            maximum = 5            
          }
      }

}

resource "google_container_node_pool" "gke_node_pool" {
  name                                      =   var.node_pool_name
  location                                  =   var.gke_location
  cluster                                   =   google_container_cluster.gke_cluster.name
  node_count                                =   var.no_of_nodes
  project                                   =   var.project_id

  node_config {
    preemptible                             =   true
    machine_type                            =   var.machine_type
    disk_size_gb                            =   var.disk_size_gb

    metadata = {
      disable-legacy-endpoints              =   "true"
    }

    service_account = google_service_account.default.email
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
      "https://www.googleapis.com/auth/devstorage.read_only",  
      "https://www.googleapis.com/auth/cloud-platform"    
    ]
  }
}


data "google_container_cluster" "clusterinfo" {
  name                                       =  var.gke_cluster_name
  location                                   =  var.gke_location
  depends_on                                 =  [google_container_cluster.gke_cluster]

}

# Data source to access the configuration of the Google Cloud provider.
data "google_client_config" "gcpconfig" {}
