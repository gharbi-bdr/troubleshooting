data "google_client_config" "provider" {}

provider "google" {
    project                     =   var.project_id
    region                      =   var.region
    credentials = file("service_account.json")
}

provider "google-beta" {
    project                     =   var.project_id
    region                      =   var.region
    credentials = file("service_account.json")
}
