output "vpc_link" {
  value       = module.vpc.vpc_link
  description = "The created network"
}

output "vpc_id" {
  value = module.vpc.vpc_id
}

output "subnetwork" {
  value = module.vpc.subnetwork
}
