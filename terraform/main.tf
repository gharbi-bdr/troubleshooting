resource "google_project_service" "compute" {
  project            = var.project_id
  service            = "compute.googleapis.com"
  disable_on_destroy = false
}


resource "google_project_service" "gke" {
  project            = var.project_id
  service            = "container.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "service_networking" {
  project            = var.project_id
  service            = "servicenetworking.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "cloudresourcemanager" {
  project            = var.project_id
  service            = "cloudresourcemanager.googleapis.com"
  disable_on_destroy = false
}

resource "google_project_service" "sqladmin" {
  project            = var.project_id
  service            = "sqladmin.googleapis.com"
  disable_on_destroy = false
}

module "vpc" {
    source                      =   "./modules/vpc"
    project_id                  =   var.project_id  
    region                      =   var.region
    envprefix                   =   var.envprefix 
    depends_on       = [
      google_project_service.service_networking,
      google_project_service.cloudresourcemanager,
    ]              
}

module "app_gke" {
    source                      =   "./modules/gke"
    project_id                  =   var.project_id
    gke_cluster_name            =   "${var.envprefix}-gke"
    region                      =   var.region
    gke_location                =   var.gke_location
    network                     =   module.vpc.vpc_link
    subnetwork                  =   module.vpc.subnetwork   
    no_of_nodes                 =   1
    machine_type                =   "e2-standard-4"
    disk_size_gb                =   "50"
    node_pool_name              =   "${var.envprefix}-nodepool"
    envprefix                   =   var.envprefix
    depends_on       = [
      google_project_service.compute,
      google_project_service.gke,
    ]
}

module "prod_app_on_gke" {
    source                      =   "./modules/kubernetes"
    access_token                =   module.app_gke.gke_access_token
    cluster_ca_certificate      =   module.app_gke.cluster_ca_certificate
    gke_endpoint                =   module.app_gke.gke_endpoint 
    gke_cluster_name            =   "${var.envprefix}-gke"
    wait_for_gke_setup          =   module.app_gke.gcp_node_pool_id
}

// module "app_pgsql" {
//     source                      =   "./modules/cloudsql"
//     project_id                  =   var.project_id
//     region                      =   var.region
//     network_name                =   module.vpc.network_name 
//     vpc_id                      =   module.vpc.vpc_id
//     machine_type                =   "db-f1-micro"
//     disk_size_gb                =   "50"
//     envprefix                   =   var.envprefix
//     pgsql_password              =   var.pgsql_password
//     depends_on       = [
//       google_project_service.sqladmin,
//     ]
// }
